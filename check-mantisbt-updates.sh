#!/bin/bash
# Script para verificar si existe una nueva versión de Mantis Back Tracking

#Cambiar por el nombre del servidor registrado en Zabbix
SrvHost=$( cat /etc/zabbix/zabbix_agent2.conf | grep -v '^#' | awk -F\= '/Hostname/ { print $2 }' )

Sender="/usr/bin/zabbix_sender"
Senderarg1='-vv'
Senderarg2='-c'
Senderarg3="/etc/zabbix/zabbix_agent2.conf"
Senderarg4='-i'
Senderarg5='-k'
Senderarg6='-o'
Senderarg7='0'
Senderarg8='1'

SenderargVersion='/tmp/zabbix/MantisBT-version.txt'

function check_version {
    PATH_INST="/var/www/html/mantis/"
    URL="https://api.github.com/repos/mantisbt/mantisbt/tags"
    
    VERSION=$( grep -w 'MANTIS_VERSION' $PATH_INST/core/constant_inc.php | awk '{ print $3}' | sed "s/'\(.*\)+.*/\1/" | tr -dc '0-9' )
    NEW_VERSION=$( curl -s $URL | grep -Po '"name":.*?[^\\]",' | egrep -vi 'RC|alpha|beta' | awk -F\: '{ print $2}' | sed 's/"\(.*\)".*/\1/' | head -1 | tr -dc '0-9' )
   
    echo $VERSION
    echo $NEW_VERSION
    vcount=0
    if [ "$VERSION" -lt "$NEW_VERSION" ]; then
        vcount=1
    fi
    echo "- MantisBT.updates "$vcount > $SenderargVersion
}

function sender_zabbix {
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargVersion -s "$SrvHost"
}
    
function main(){
    mkdir -p /tmp/zabbix
    check_version
    sender_zabbix
}

main
