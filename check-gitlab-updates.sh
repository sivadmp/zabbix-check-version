#!/bin/bash
# Script para verificar si exise una nueva versión de gitlab

# Doc https://docs.gitlab.com/ee/api/tags.html
# Project: https://gitlab.com/gitlab-org/gitlab-foss
# ID: 13083
# https://gitlab.com/api/v4/projects/13083/repository/tags

#Cambiar por el nombre del servidor registrado en Zabbix
SrvHost=$( cat /etc/zabbix/zabbix_agent2.conf | grep -v '^#' | awk -F\= '/Hostname/ { print $2 }' )

Sender="/usr/bin/zabbix_sender"
Senderarg1='-vv'
Senderarg2='-c'
Senderarg3="/etc/zabbix/zabbix_agent2.conf"
Senderarg4='-i'
Senderarg5='-k'
Senderarg6='-o'
Senderarg7='0'
Senderarg8='1'

SenderargVersion='/tmp/zabbix/version.txt'

function check_version {
    URL_GITLAB="https://gitlab.com/api/v4/projects/13083/repository/tags"
    
    VERSION_GITLAB=$( grep gitlab-ce /opt/gitlab/version-manifest.txt | awk '{ print $2 }' | tr -dc '0-9' )
    NEW_VERSION_GITLAB=$( curl -s $URL_GITLAB | grep -Po '"name":.*?[^\\]",' | egrep -vi 'RC|alpha|beta' | awk -F\: '{ print $2}' | sed 's/^"\(.*\)".*/\1/' | sort -r | head -1 | tr -dc '0-9')
   
    vcount=0
    if [ "$VERSION_GITLAB" -lt "$NEW_VERSION_GITLAB" ]; then
        vcount=1
    fi
    echo "- Gitlab.updates "$vcount > $SenderargVersion
}

function sender_zabbix {
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargVersion -s "$SrvHost"
}
    
function main(){
    mkdir -p /tmp/zabbix
    check_version
    sender_zabbix
}

main
