# Zabbix

Repositorio que contiene plantillas de Zabbix para monitorear actualizaciones de sistemas instalados. Ha sido probado en Zabbix v5.2.7 con Debian 10 y 11. 

## Requisitos

- Solo se ha probado con agentes activos Zabbix.
- Tener instalado zabbix-sender

## Sistemas o servicios monitoreados

* **Gitlab**
  - **gitlab.xml:** Plantilla de Gitlab para importar a zabbix
  - **check-gitlab-updates.sh:** script schell para identificar si existe una nueva versión del sistema instalado

* **OwnCloud**
  - **owncloud.xml:** Plantilla de owncloud para importar a zabbix
  - **check-owncloud-updates.sh:** script schell para identificar si existe una nueva versión del sistema instalado

* **Moodle**
  - **moodle.xml:** Plantilla de Moodle para importar a zabbix
  - **check-moodle-updates.sh:** script schell para identificar si existe una nueva versión del sistema instalado

* **Mantis Bug Tracker**
  - **owncloud.xml:** Plantilla de mantisBT para importar a zabbix
  - **check-owncloud-updates.sh:** script schell para identificar si existe una nueva versión del sistema instalado

## Instalación

Instalando Zabbix-sender

```sh
sudo apt install zabbix-sender
```

## Configuración

Habilitar la ejecución de comandos remotos 

> https://www.zabbix.com/documentation/5.2/en/manual/config/items/restrict_checks
 
Editar el archivo de configuración
```sh
sudo nano /etc/zabbix/zabbix_agent2.conf
```

Adicionar la siguiente linea en la configuración
```
AllowKey=system.run[*]
```

Para que los cambios surtan efecto, reiniciar el servicio
```sh
sudo systemctl restart zabbix-agent2.service
```

Crear el directorio de plugins
```sh
sudo mkdir /etc/zabbix/plugins
```

Descargar el script y copiar a la carpeta creada
```sh
mv check-gitlab-updates.sh /etc/zabbix/plugins
```

Asignarle permisos de ejecución al script creado
```sh
chmod 755 /etc/zabbix/plugins/check-gitlab-updates.sh
```

## Probar 

Para verificar si el script funciona correctamente, realizar la prueba ejecutando
```sh
sudo -u zabbix -H bash /etc/zabbix/plugins/check-gitlab-updates.sh
```

El resultado del comando nos muestra lo siguiente:
```sh
zabbix_sender [14058]: DEBUG: answer [{"response":"success","info":"processed: 1; failed: 0; total: 1; seconds spent: 0.000057"}]
info from server: "processed: 1; failed: 0; total: 1; seconds spent: 0.000057"
sent: 1; skipped: 0; total: 1
```

Del resultado obtenido, se puede identificar el resultado correcto con **0**  fallas
```sh
"response":"success","info":"processed: 1; failed: 0
```
