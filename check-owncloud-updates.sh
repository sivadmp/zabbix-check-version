#!/bin/bash
# Script para verificar si existe una nueva versión de OwnCloud

#Cambiar por el nombre del servidor registrado en Zabbix
SrvHost=$( cat /etc/zabbix/zabbix_agent2.conf | grep -v '^#' | awk -F\= '/Hostname/ { print $2 }' )

Sender="/usr/bin/zabbix_sender"
Senderarg1='-vv'
Senderarg2='-c'
Senderarg3="/etc/zabbix/zabbix_agent2.conf"
Senderarg4='-i'
Senderarg5='-k'
Senderarg6='-o'
Senderarg7='0'
Senderarg8='1'

SenderargVersion='/tmp/zabbix/ownCloud-version.txt'

function check_version {
    PATH_INST="/var/www/html/owncloud/"
    URL="https://api.github.com/repos/owncloud/core/tags"
    
    VERSION=$( grep -w '$OC_VersionString' $PATH_INST/version.php | awk -F\= '{ print $2}'| sed "s/'\(.*\)'.*/\1/" | tr -dc '0-9' )
    NEW_VERSION=$( curl -s $URL | grep -Po '"name":.*?[^\\]",' | egrep -vi 'RC|alpha|beta' | awk -F\: '{ print $2}' | sed 's/^"\(.*\)".*/\1/' | head -1 | tr -dc '0-9' )
   
    vcount=0
    if [ "$VERSION" -lt "$NEW_VERSION" ]; then
        vcount=1
    fi
    echo "- OwnCloud.updates "$vcount > $SenderargVersion
}

function sender_zabbix {
    $Sender $Senderarg1 $Senderarg2 $Senderarg3 $Senderarg4 $SenderargVersion -s "$SrvHost"
}
    
function main(){
    mkdir -p /tmp/zabbix
    check_version
    sender_zabbix
}

main
